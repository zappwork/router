package router

import "fmt"

// NewGroup returns a point to a route group
func NewGroup(path string) *RouteGroup {
	g := &RouteGroup{}
	g.SetPath(path)
	return g
}

// RouteGroup is grouping routes
type RouteGroup struct {
	path   string
	routes map[string]map[string]route
	use    []Func
	groups []*RouteGroup
}

// Path get the group path
func (g *RouteGroup) Path() string {
	return g.path
}

// SetPath set the group path
func (g *RouteGroup) SetPath(path string) {
	if path == "" || path[len(path)-1:] != basePath {
		path = fmt.Sprintf("%s/", path)
	}
	g.path = path
}

// Get add a new route GET route to the router and specify the handling function
// and/or controller
func (g *RouteGroup) Get(path string, f Func, use ...Func) *RouteGroup {
	g.add("GET", path, f, use...)
	return g
}

// Post add a new route POST route to the router and specify the handling function
// and/or controller
func (g *RouteGroup) Post(path string, f Func, use ...Func) *RouteGroup {
	g.add("POST", path, f, use...)
	return g
}

// Put add a new route PUT route to the router and specify the handling function
// and/or controller
func (g *RouteGroup) Put(path string, f Func, use ...Func) *RouteGroup {
	g.add("PUT", path, f, use...)
	return g
}

// Delete add a new route DELETE route to the router and specify the handling function
// and/or controller
func (g *RouteGroup) Delete(path string, f Func, use ...Func) *RouteGroup {
	g.add("DELETE", path, f, use...)
	return g
}

// Options add a new route OPTIONS route to the router and specify the handling function
// and/or controller
func (g *RouteGroup) Options(path string, f Func, use ...Func) *RouteGroup {
	g.add("OPTIONS", path, f, use...)
	return g
}

// Patch add a new route PATCH route to the router and specify the handling function
// and/or controller
func (g *RouteGroup) Patch(path string, f Func, use ...Func) *RouteGroup {
	g.add("PATCH", path, f, use...)
	return g
}

// Catch a route independent of the request method and specify the handling function
// and/or controller
func (g *RouteGroup) Catch(path string, f Func, use ...Func) *RouteGroup {
	g.add("CATCH", path, f, use...)
	return g
}

// Use add middleware specific for this group
func (g *RouteGroup) Use(f ...Func) *RouteGroup {
	g.use = append(g.use, f...)
	return g
}

// Group add a grouped route on the given path
func (g *RouteGroup) Group(gr *RouteGroup) *RouteGroup {
	g.groups = append(g.groups, gr)
	return g
}

// Add route to the group
func (g *RouteGroup) add(method, path string, f Func, use ...Func) {
	// Use group base paths
	if path == "." || path == "../" {
		path = "/"
	}
	if g.routes == nil {
		g.routes = make(map[string]map[string]route)
	}
	if g.routes[method] == nil {
		g.routes[method] = make(map[string]route)
	}
	g.routes[method][path] = route{handler: f, use: use}
}

// addToRouter adds all the routes in the group to the router with as base the given path
func (g RouteGroup) addToRouter(rt *Router) {
	// Add all the subgroups
	for _, gr := range g.groups {
		if len(g.use) > 0 {
			gr.Use(g.use...)
		}
		relativePath := gr.Path()
		path := relativePath
		if path[:1] == basePath {
			path = path[1:]
		}
		path = g.Path() + path
		gr.SetPath(path)
		gr.addToRouter(rt)
		gr.SetPath(relativePath)
	}

	// Add all the groups routes
	for m, r := range g.routes {
		for p, r := range r {
			if p[:1] == basePath {
				p = g.path + p[1:]
			} else {
				p = g.path + p
			}
			u := append(g.use, r.use...)
			if m == "CATCH" {
				rt.Catch(p, r.handler, u...)
				continue
			}
			rt.add(m, p, r.handler, u...)
		}
	}
}
