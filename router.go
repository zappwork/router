// Package router The zappwork Golang router and is an easy to use router with
// pre/post processing middleware.
//
// Serving static files
//
// Serving static files is easy, see the example below.
//
// 	package main
// 	import (
// 		"bitbucket.org/zappwork/router"
// 	)
// 	func main() {
//		rt := NewRouter(nil)
// 		rt.StaticFile("/",             "./assets/index.html")
// 		rt.StaticFile("/favicon.ico",  "./assets/favicon.ico")
// 		rt.StaticFolder("/css/",       "./assets/css/")
// 		rt.StaticFolder("/js/",        "./assets/js/")
// 		rt.StaticFolder("/img/",       "./assets/img/")
// 		rt.Start(nil)
// 	}
//
// Some examples of how to use the routing
//
// Catch request for file.html and request method GET
// 	rt.Get("/file.html", func(r router.RouteRequest) (err *router.Error) {
// 		fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
// 		return
// 	})
// Catch request for file.html and request method POST
// 	rt.Post("/file.html", func(r router.RouteRequest) (err *router.Error) {
// 		fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
// 		return
// 	})
// Catch request for file.html and request method PUT
// 	rt.Put("/file.html", func(r router.RouteRequest) (err *router.Error) {
// 		fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
// 		return
// 	})
// Catch request for file.html and request method DELETE
// 	rt.Delete("/file.html", func(r router.RouteRequest) (err *router.Error) {
// 		fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
// 		return
// 	})
// Catch request for file.html and request method OPTIONS
// 	rt.Options("/file.html", func(r router.RouteRequest) (err *router.Error) {
// 		fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
// 		return
// 	})
// Catch request for file.html and request method PATCH
// 	rt.Patch("/file.html", func(r router.RouteRequest) (err *router.Error) {
// 		fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
// 		return
// 	})
package router

import (
	"fmt"
	"mime"
	"net"
	"net/http"
	"os"
	"os/signal"
	"path/filepath"
	"regexp"
	"runtime/debug"
	"sort"
	"strings"
	"syscall"
	"time"
)

const basePath = "/"

var (
	totalRouters = 0
)

// Func is the generic router function used in the application
type Func func(*RouteRequest) *Error

// NewRouter return initialized router
func NewRouter(c *Config) *Router {
	totalRouters = totalRouters + 1
	r := &Router{
		id:     totalRouters,
		logger: DefaultLogger{},
	}
	r.UUID = fmt.Sprintf("%03d", r.id)
	if c != nil {
		r.config = c
	} else {
		r.config = NewConfig()
	}
	r.Reset()
	return r
}

// Router is a new Router instance
type Router struct {
	id int
	// Unique number for the current router
	UUID string

	// Router configuration
	config *Config

	mux *http.ServeMux

	// Use handlers before going in the route functions
	use []Func

	// Custom not found function handler. This will override the default
	// http.NotFound() handler.
	notFoundHandler Func

	// Catch all handler, which catches all unhandled routes
	catchAll Func

	// Custom error function handler. This will override the default
	// http.Error() handler.
	errorHandler func(http.ResponseWriter, *http.Request, *Error)

	// Map contain all static routes based on request method, path depth and the actual path
	staticRoutes map[string]map[int]map[string]route
	// Map contain all dynamic routes based on request method, path depth and the actual path
	dynamicRoutes   map[string]map[int]map[int][]route
	dynamicArgCount []int
	// Add catchAll static routes
	allStaticRoutes map[int]map[string]route
	// Add catchAll static routes
	allDynamicRoutes map[int]map[int][]route

	// OnStart custom start funcion else the default start message will be displayed
	OnStart func()
	// OnStop custom stop function else the default stop message will be displayed
	OnStop func(string)
	// logfunc can be set for custom logging. Type router.SetLogger(func(method string, path string, status int, ip string, useragent string, duration string, err error) {
	// 		//handle the log information here.
	// }
	logger Logger

	// Counters
	staticRoutesCount  int
	dynamicRoutesCount int
	staticFilesCount   int
	staticFoldersCount int
}

// Reset all routes and validations
func (r *Router) Reset() {
	r.mux = http.NewServeMux()
	r.staticRoutes = make(map[string]map[int]map[string]route)
	r.dynamicRoutes = make(map[string]map[int]map[int][]route)
	r.allStaticRoutes = make(map[int]map[string]route)
	r.allDynamicRoutes = make(map[int]map[int][]route)
	r.staticRoutesCount = 0
	r.dynamicRoutesCount = 0
	r.staticFilesCount = 0
	r.staticFoldersCount = 0
	r.use = nil
}

// Route container for storing routes in the router
type route struct {
	path       string
	handler    Func
	use        []Func
	matcher    *regexp.Regexp
	properties []string
}

// Create and return a dynamic route
func dynamicRoute(path string, f Func, use []Func) route {
	reg, _ := regexp.Compile("{.*?}")
	rexp := reg.ReplaceAllString(path, "(.*?)")
	rexp = strings.Replace(rexp, ").(", ")\\.(", -1)
	// Create regex for matching
	rtm, err := regexp.Compile("^" + rexp + "$")
	if err != nil {
		panic(err)
	}
	return route{
		path:       path,
		handler:    f,
		use:        use,
		matcher:    rtm,
		properties: reg.FindAllString(path, -1),
	}
}

// Use add pre route processing. This will enable you to add pre route processing
// like route rewriting/authentication.
func (r *Router) Use(f Func) *Router {
	r.use = append(r.use, f)
	return r
}

// StaticFile add a single file to serve through the router.
func (r *Router) StaticFile(path string, serve string) *Router {
	if path[:1] != basePath {
		path = basePath + path
	}
	if serve[:1] != basePath && serve[:2] != "./" {
		serve = "./" + serve
	}
	r.staticFilesCount++
	r.mux.HandleFunc(path, func(w http.ResponseWriter, rq *http.Request) {
		t := time.Now()
		r.setCacheHeaders(w)
		http.ServeFile(w, rq, serve)
		r.logRequest(200, rq, nil, t)
	})
	return r
}

// StaticFolder add static folder paths. This should be folders and paths will
// be cleaned correctly. And example would be:
// rt.AddStatic("/js/",  "./assets/default/js")
// The first argument is the base path as defined in the html and
// the seconds is the path to the files on the server.
func (r *Router) StaticFolder(base string, serve string) *Router {
	if base[:1] != basePath {
		base = basePath + base
	}
	if base[len(base)-1:] != basePath {
		base = base + basePath
	}
	if serve[:1] != basePath && serve[:2] != "./" {
		serve = "./" + serve
	}
	if serve[len(serve)-1:] == basePath {
		serve = serve[0 : len(serve)-1]
	}
	r.staticFoldersCount++
	handler := http.StripPrefix(base, http.FileServer(http.Dir(serve)))
	r.mux.Handle(base, http.HandlerFunc(func(w http.ResponseWriter, rq *http.Request) {
		t := time.Now()
		r.setCacheHeaders(w)
		handler.ServeHTTP(w, rq)
		r.logRequest(200, rq, nil, t)
	}))
	return r
}

// Get add a new route GET route to the router and specify the handling function
// and/or controller
func (r *Router) Get(path string, f Func, use ...Func) *Router {
	r.add("GET", path, f, use...)
	return r
}

// Post add a new route POST route to the router and specify the handling function
// and/or controller
func (r *Router) Post(path string, f Func, use ...Func) *Router {
	r.add("POST", path, f, use...)
	return r
}

// Put add a new route PUT route to the router and specify the handling function
// and/or controller
func (r *Router) Put(path string, f Func, use ...Func) *Router {
	r.add("PUT", path, f, use...)
	return r
}

// Delete add a new route DELETE route to the router and specify the handling function
// and/or controller
func (r *Router) Delete(path string, f Func, use ...Func) *Router {
	r.add("DELETE", path, f, use...)
	return r
}

// Options add a new route OPTIONS route to the router and specify the handling function
// and/or controller
func (r *Router) Options(path string, f Func, use ...Func) *Router {
	r.add("OPTIONS", path, f, use...)
	return r
}

// Patch add a new route PATCH route to the router and specify the handling function
// and/or controller
func (r *Router) Patch(path string, f Func, use ...Func) *Router {
	r.add("PATCH", path, f, use...)
	return r
}

// Catch a route independent of the request method and specify the handling function
// and/or controller
func (r *Router) Catch(path string, f Func, use ...Func) *Router {
	if path[:1] != basePath {
		path = basePath + path
	}
	if filepath.Ext(path) == "" && path[len(path)-1:] != basePath {
		path = path + basePath
	}
	depth := depth(path)
	numArg := strings.Count(path, "{")
	if numArg == 0 {
		if r.allStaticRoutes[depth] == nil {
			r.allStaticRoutes[depth] = make(map[string]route)
		}
		r.allStaticRoutes[depth][path] = route{handler: f, use: use}
	} else {
		if r.allDynamicRoutes[depth] == nil {
			r.allDynamicRoutes[depth] = make(map[int][]route)
		}
		if _, ok := r.allDynamicRoutes[depth][numArg]; !ok {
			r.dynamicArgCount = append(r.dynamicArgCount, numArg)
		}
		r.allDynamicRoutes[depth][numArg] = append(r.allDynamicRoutes[depth][numArg], dynamicRoute(path, f, use))
	}
	return r
}

// Group add a grouped route on the given path
func (r *Router) Group(g *RouteGroup) *Router {
	g.addToRouter(r)
	return r
}

// Add a new route to the router and specify the handling function
// and/or controller
func (r *Router) add(method string, path string, f Func, use ...Func) {
	if path[:1] != basePath {
		path = basePath + path
	}
	if filepath.Ext(path) == "" && path[len(path)-1:] != basePath {
		path = path + basePath
	}
	depth := depth(path)
	numArg := strings.Count(path, "{")
	if numArg == 0 {
		if r.staticRoutes[method] == nil {
			r.staticRoutes[method] = make(map[int]map[string]route)
		}
		if r.staticRoutes[method][depth] == nil {
			r.staticRoutes[method][depth] = make(map[string]route)
		}
		r.staticRoutes[method][depth][path] = route{handler: f, use: use}
		r.staticRoutesCount++
	} else {
		if r.dynamicRoutes[method] == nil {
			r.dynamicRoutes[method] = make(map[int]map[int][]route)
		}
		if r.dynamicRoutes[method][depth] == nil {
			r.dynamicRoutes[method][depth] = make(map[int][]route)
		}
		if _, ok := r.dynamicRoutes[method][depth][numArg]; !ok {
			r.dynamicArgCount = append(r.dynamicArgCount, numArg)
		}
		r.dynamicRoutes[method][depth][numArg] = append(r.dynamicRoutes[method][depth][numArg], dynamicRoute(path, f, use))
		r.dynamicRoutesCount++
	}
}

// Start the router and start listening for connections
func (r *Router) Start(c *Config, keepAlive bool) {
	if c != nil {
		r.config = c
	}
	if r.OnStart != nil {
		r.OnStart()
	} else if r.config.ShowSystemInfo {
		r.showWelcome()
	} else {
		r.logger.Infof("Router %s started and listening on %s", r.UUID, r.config.HostString())
	}

	// Sort the dynamicArgCount for checking the paths with the least arguments first..
	sort.Ints(r.dynamicArgCount)

	// On force stop call the stop function
	shutdown := make(chan os.Signal, 1)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)
	go func() {
		for message := range shutdown {
			r.stop(message)
		}
	}()

	if keepAlive {
		err := http.ListenAndServe(r.config.HostString(), r.Handler())
		if err != nil {
			r.logger.Error(err)
		}
	} else {
		go func() {
			http.ListenAndServe(r.config.HostString(), r.Handler())
		}()
	}
}

// Stop the router and display the message
func (r *Router) Stop(message string) {
	fmt.Println("")
	if r.OnStop != nil {
		r.OnStop(message)
	} else if r.config.ShowSystemInfo {
		r.showGoodbye(message)
	} else {
		r.logger.Infof("Router %s stopped with %s", r.UUID, message)
	}
	os.Exit(0)
}

// Stop the router and display the message
func (r *Router) stop(message os.Signal) {
	r.Stop(message.String())
}

// Handler return the http.Handler for the router
func (r *Router) Handler() (handler http.Handler) {
	// Add handle for dynamic routing
	r.mux.HandleFunc(basePath, r.handleRoutes)
	// Set up request timeout
	handler = r.mux
	if int64(r.config.RequestTimeout) > 0 {
		handler = http.TimeoutHandler(handler, r.config.RequestTimeout, r.config.TimeoutMessage)
	}
	return handler
}

// handleRoutes the handler function for the main route handling. This is not for
// static file and folder handling.
func (r *Router) handleRoutes(w http.ResponseWriter, rq *http.Request) {
	// Set the timer
	t := time.Now()
	// Define the intial RouteRequest
	rr := &RouteRequest{w: w, r: rq}

	// Go through the pre processors
	for _, u := range r.use {
		if err := u(rr); err != nil {
			r.handleError(w, rq, t, err)
			return
		}
	}

	// Get the path and clean it
	path := rq.URL.Path
	if filepath.Ext(path) == "" && path[len(path)-1:] != basePath {
		path = path + basePath
	}
	// Get path depth
	depth := depth(path)
	// Get the request method
	method := requestMethod(rq)
	// theme := GetTheme(r.Host)

	if r.config.CorsEnabled {
		rr.AddHeader("Access-Control-Allow-Origin", r.config.CorsAllowOrigin)
		rr.AddHeader("Access-Control-Allow-Headers", r.config.CorsAllowHeaders)
		rr.AddHeader("Access-Control-Allow-Methods", r.config.CorsAllowMethods)
		if method == "OPTIONS" {
			rr.Write("")
			return
		}
	}

	// Check static routes for given method
	if rt, ok := r.staticRoutes[method][depth][path]; ok {
		r.handleFunc(rt, rr, t)
		return
	}

	// Check dynamic routes for given method
	if routes, ok := r.dynamicRoutes[method][depth]; ok {
		// Go through dynamic routes to find a match
		for _, a := range r.dynamicArgCount {
			for _, rt := range routes[a] {
				if match, args := match(rt, path); match {
					rr.args = args
					r.handleFunc(rt, rr, t)
					return
				}
			}
		}
	}

	if rt, ok := r.allStaticRoutes[depth][path]; ok {
		r.handleFunc(rt, rr, t)
		return
	}

	// Check dynamic routes for given method
	if routes, ok := r.allDynamicRoutes[depth]; ok {
		// Go through dynamic routes to find a match
		for _, a := range r.dynamicArgCount {
			for _, rt := range routes[a] {
				if match, args := match(rt, path); match {
					rr.args = args
					r.handleFunc(rt, rr, t)
					return
				}
			}
		}
	}

	// Nothing found so display a not found page
	if r.catchAll != nil {
		setContentType(rr.Writer(), rr.Path())
		if err := r.catchAll(rr); err != nil {
			r.handleError(rr.Writer(), rr.Request(), t, err)
			r.logRequest(500, rr.Request(), nil, t)
		} else {
			r.logRequest(200, rr.Request(), nil, t)
		}
	} else {
		r.handleNotFound(rr, t)
	}
}

// Generic handle route function. This will handle all routes which were provide
// through Router.Add().
func (r *Router) handleFunc(rt route, rr *RouteRequest, t time.Time) {
	// Recover from a panic attack ;-)
	defer func() {
		if rec := recover(); rec != nil {
			r.handleError(rr.Writer(), rr.Request(), t, NewError(500, fmt.Sprintf("%v", rec)))
		}
	}()

	setContentType(rr.Writer(), rr.Path())
	// Check for use in route
	if rt.use != nil && len(rt.use) > 0 {
		for _, u := range rt.use {
			if err := u(rr); err != nil {
				r.handleError(rr.Writer(), rr.Request(), t, err)
				return
			}
		}
	}
	if err := rt.handler(rr); err != nil {
		r.handleError(rr.Writer(), rr.Request(), t, err)
	} else {
		status := 200
		if s := rr.StatusCode(); s > 0 {
			status = s
		}
		r.logRequest(status, rr.Request(), nil, t)
	}
}

// AddNotFoundHandler add a custom NotFound handler to the router. This will enable
// you serve custom 404 pages.
func (r *Router) AddNotFoundHandler(handler Func) {
	r.notFoundHandler = handler
}

// AddCatchAllHandler add a catch all unhandeled router to the router.
func (r *Router) AddCatchAllHandler(handler Func) {
	r.catchAll = handler
}

// HandleNotFound handle not found request in the routing. If there is no custom not found handler provided then
// the default notfound response will be generated.
func (r *Router) handleNotFound(rr *RouteRequest, t time.Time) {
	if r.notFoundHandler == nil {
		http.NotFound(rr.Writer(), rr.Request())
	} else {
		setContentType(rr.Writer(), rr.Request().URL.Path)
		rr.Writer().WriteHeader(404)
		r.notFoundHandler(rr)
	}
	r.logRequest(404, rr.Request(), nil, t)
}

// AddErrorHandler add custom error handler. This will enable you to show custom
// 500 error pages.
func (r *Router) AddErrorHandler(handler func(http.ResponseWriter, *http.Request, *Error)) {
	r.errorHandler = handler
}

// HandleError handle errors in the routing. If there is no custom error handler provided then
// the default error will be generated.
func (r *Router) handleError(w http.ResponseWriter, rq *http.Request, t time.Time, err *Error) {
	code := 500
	message := ""
	if err != nil {
		code = err.Code
		message = err.Message
	} else {
		message = http.StatusText(code)
	}
	if r.errorHandler == nil {
		http.Error(w, message, code)
	} else {
		setContentType(w, rq.URL.Path)
		w.WriteHeader(code)
		r.errorHandler(w, rq, err)
	}
	r.logRequest(code, rq, err, t)
	if r.config.Verbose {
		debug.PrintStack()
	}
}

// SetLogger sets a custom logging function
func (r *Router) SetLogger(l Logger) {
	r.config.Logging = true
	r.logger = l
}

// Log the request
func (r *Router) logRequest(status int, rq *http.Request, err *Error, t time.Time) {
	if !r.config.Logging {
		return
	}
	r.logger.Request(r.UUID, requestMethod(rq), rq.URL.RequestURI(), status, time.Since(t).String(), ParseRemoteAddr(rq), rq.UserAgent(), err)
}

// Show the default Welcome message
func (r *Router) showWelcome() {
	lLen := 70
	l := fmt.Sprintf("------ %%-%ds ------\n", lLen)
	tLen := lLen + 14
	fmt.Println(strings.Repeat("-", tLen))
	fmt.Printf(l, cstring(fmt.Sprintf("ZAPPWORK Router %s initialising", r.UUID), lLen))
	fmt.Println(strings.Repeat("-", tLen))
	if r.staticFilesCount > 0 {
		fmt.Printf(l, fmt.Sprintf("%d static files served", r.staticFilesCount))
	}
	if r.staticFoldersCount > 0 {
		fmt.Printf(l, fmt.Sprintf("%d static folders served", r.staticFoldersCount))
	}
	if r.staticRoutesCount > 0 {
		fmt.Printf(l, fmt.Sprintf("%d static routes initialised", r.staticRoutesCount))
	}
	if r.dynamicRoutesCount > 0 {
		fmt.Printf(l, fmt.Sprintf("%d dynamic routes initialised", r.dynamicRoutesCount))
	}
	fmt.Printf(l, "")
	fmt.Printf(l, cstring("Configuration", lLen))
	for _, cl := range r.config.Strings() {
		fmt.Printf(l, cl)
	}
	fmt.Println(strings.Repeat("-", tLen))
	fmt.Printf(l, cstring(fmt.Sprintf("ZAPPWORK Router %s started and listening", r.UUID), lLen))
	fmt.Println(strings.Repeat("-", tLen))
}

// Show the default goodbye message
func (r *Router) showGoodbye(message string) {
	lLen := 70
	l := fmt.Sprintf("------ %%-%ds ------\n", lLen)
	tLen := lLen + 14
	fmt.Println(strings.Repeat("-", tLen))
	fmt.Printf(l, cstring(fmt.Sprintf("ZAPPWORK Router %s stopped with %s", r.UUID, message), lLen))
	fmt.Println(strings.Repeat("-", tLen))
}

// setContentType Set the content type according to the extension of the given path
func setContentType(w http.ResponseWriter, path string) {
	ext := filepath.Ext(path)
	if ext == "" {
		ext = ".html"
	}
	m := mime.TypeByExtension(ext)
	w.Header().Set("Content-Type", m)
}

// setCacheHeaders set headers for caching
func (r *Router) setCacheHeaders(w http.ResponseWriter) {
	if r.config.StaticTimeout > 0 {
		w.Header().Add("Cache-Control", fmt.Sprintf("max-age=%d, public", r.config.StaticTimeout))
	}
}

// Get the request method
func requestMethod(r *http.Request) (m string) {
	m = r.Method
	if r.FormValue("_METHOD") != "" {
		m = r.FormValue("_METHOD")
	} else if r.Header.Get("X-HTTP-Method-Override") != "" {
		m = r.Header.Get("X-HTTP-Method-Override")
	}
	m = strings.ToUpper(m)
	return
}

// Get the accept type
func acceptType(r *http.Request) string {
	accept := r.Header.Get("accept")
	if strings.Contains(accept, "json") {
		return "json"
	}
	if strings.Contains(accept, "javascript") {
		return "javascript"
	}
	if strings.Contains(accept, "css") {
		return "css"
	}
	if strings.Contains(accept, "html") {
		return "html"
	}
	return "xml"
}

// Get the depth of the supplied path. This is to store the
// the paths by depth for faster handling
func depth(path string) int {
	return strings.Count(path, basePath) - 1
}

// Match the dynamic route to the provided path
// and create a property value map.
func match(rt route, path string) (match bool, args map[string]string) {
	// Does the route match?
	match = rt.matcher.MatchString(path)
	if !match {
		return
	}
	// Get the properties if needed
	args = make(map[string]string)
	values := rt.matcher.FindAllStringSubmatch(path, -1)
	for i := 0; i < len(rt.properties); i++ {
		args[rt.properties[i][1:len(rt.properties[i])-1]] = values[0][i+1]
	}
	return
}

// ParseRemoteAddr parse a remoteAddr to a human readable ip address
func ParseRemoteAddr(r *http.Request) (ipAddr string) {
	ip, _, err := net.SplitHostPort(r.RemoteAddr)
	if err != nil {
		return
	}
	userIP := net.ParseIP(ip)
	if userIP == nil {
		ipAddr = ip
		return
	}
	ipAddr = userIP.String()
	return
}

// Center a string within spaces
func cstring(s string, width int) string {
	plen := (width - len(s)) / 2
	return strings.Repeat(" ", plen) + s
}
