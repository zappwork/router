package router

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/cookiejar"
	"net/http/httptest"
	"net/url"
	"reflect"
	"regexp"
	"strings"
	"testing"
	"time"
)

func TestQueryParams(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		if r.Method() != "GET" {
			return NewError(500, "Incorrect request method")
		}
		r.SetStatusCode(200)
		if !r.HasQueryParam("i") {
			t.Error("HasQueryParam param not working")
			r.SetStatusCode(500)
		}
		if i, ok := r.QueryParamAsInt64("i"); ok {
			if i != 13 {
				t.Error("QueryParamAsInt64 incorrect value")
				r.SetStatusCode(500)
			}
		} else {
			t.Error("QueryParamAsInt64 param not working")
			r.SetStatusCode(500)
		}
		if _, ok := r.QueryParamAsInt64("in"); ok {
			t.Error("QueryParamAsInt64 param not working")
			r.SetStatusCode(500)
		}
		if f, ok := r.QueryParamAsFloat64("f"); ok {
			if f != 0.55 {
				t.Error("QueryParamAsFloat64 incorrect value")
				r.SetStatusCode(500)
			}
		} else {
			t.Error("QueryParamAsFloat64 param not working")
			r.SetStatusCode(500)
		}
		if _, ok := r.QueryParamAsFloat64("in"); ok {
			t.Error("QueryParamAsFloat64 param not working")
			r.SetStatusCode(500)
		}
		if b, ok := r.QueryParamAsBool("b"); ok {
			if !b {
				t.Error("QueryParamAsBool incorrect value")
				r.SetStatusCode(500)
			}
		} else {
			t.Error("QueryParamAsBool param not working")
			r.SetStatusCode(500)
		}
		if _, ok := r.QueryParamAsBool("in"); ok {
			t.Error("QueryParamAsBool param not working")
			r.SetStatusCode(500)
		}
		if s, ok := r.QueryParamAsSlice("s", ","); ok {
			if !reflect.DeepEqual(s, []string{"1", "2", "3", "4"}) {
				t.Error("QueryParamAsSlice incorrect value")
				r.SetStatusCode(500)
			}
		} else {
			t.Error("QueryParamAsSlice param not working")
			r.SetStatusCode(500)
		}
		if _, ok := r.QueryParamAsSlice("in", ","); ok {
			t.Error("QueryParamAsSlice param not working")
			r.SetStatusCode(500)
		}
		if str, ok := r.QueryParam("str"); ok {
			if str != "test" {
				t.Error("QueryParam incorrect value")
				r.SetStatusCode(500)
			}
		} else {
			t.Error("QueryParam param not working")
			r.SetStatusCode(500)
		}
		if _, ok := r.QueryParam("in"); ok {
			t.Error("QueryParam param not working")
			r.SetStatusCode(500)
		}
		if r.StatusCode() == 500 {
			t.Fatal("Query param not working")
		}
		return r.WriteBuffer(bytes.NewBuffer([]byte(string(r.StatusCode()))))
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	ts.URL += "/test.html?f=0.55&i=13&b=true&s=1,2,3,4&str=test"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 {
		t.Fatalf("Get got status %d at %q, because 200 matching /test.html", r.StatusCode, url)
	}
}

func TestPostVarsAs(t *testing.T) {
	rt := NewRouter(nil)
	rt.AddErrorHandler(func(w http.ResponseWriter, r *http.Request, err *Error) {
		fmt.Fprintf(w, "%v", err)
	})
	rt.Post("/test.html", func(r *RouteRequest) (err *Error) {
		if s, ok := r.PostValue("string"); !ok || s != "value" {
			return NewError(500, fmt.Sprintf("Invalid string value %s", s))
		}
		if b, ok := r.PostValueAsBool("bool"); !ok || !b {
			return NewError(500, fmt.Sprintf("Invalid bool value %t", b))
		}
		if i, ok := r.PostValueAsInt("int"); !ok || i != 123 {
			return NewError(500, fmt.Sprintf("Invalid int value %d", i))
		}
		if i, ok := r.PostValueAsInt64("int"); !ok || i != 123 {
			return NewError(500, fmt.Sprintf("Invalid int64 value %d", i))
		}
		if f, ok := r.PostValueAsFloat64("float"); !ok || f != 0.55 {
			return NewError(500, fmt.Sprintf("Invalid float64 value %f", f))
		}
		if m, ok := r.PostMultiValue("multi"); !ok || !reflect.DeepEqual(m, []string{"1", "2", "3"}) {
			return NewError(500, fmt.Sprintf("Invalid multi value %v", m))
		}
		if m, ok := r.PostMap("map"); !ok || !reflect.DeepEqual(m, PostMap{"one": "1", "two": "2"}) {
			return NewError(500, fmt.Sprintf("Invalid map value %+v", m))
		}
		if m, ok := r.PostArrayMap("smap"); !ok || !reflect.DeepEqual(m, map[int]PostMap{23: {"one": "1", "two": "2"}}) {
			return NewError(500, fmt.Sprintf("Invalid smap value %+v", m))
		}
		return r.WriteBytes([]byte("test"))
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.html$")
	ts.URL += "/test.html"
	r, err := http.PostForm(ts.URL, url.Values{"string": {"value"}, "int": {"123"}, "float": {"0.55"}, "bool": {"true"}, "multi": {"1", "2", "3"}, "map[one]": {"1"}, "map[two]": {"2"}, "smap[23][one]": {"1"}, "smap[23][two]": {"2"}})
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) {
		t.Fatalf("Got status %d at %q, because 200 matching /test.html$ with body %s", r.StatusCode, url, body)
	}
}

func TestStatusCode(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		r.WriteStatusCode(401)
		fmt.Fprintf(r.Writer(), "test")
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	ts.URL += "/test.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 401 {
		t.Fatalf("Get got status %d at %q, because 401 matching /test.html", r.StatusCode, url)
	}
}

func TestHeader(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		r.WriteStatusCode(401)
		fmt.Fprintf(r.Writer(), "test")
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	ts.URL += "/test.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 401 {
		t.Fatalf("Get got status %d at %q, because 401 matching /test.html", r.StatusCode, url)
	}
}

type TestType struct {
	One string
	Two string
}

func TestContext(t *testing.T) {
	r := RouteRequest{}
	r.SetContext("test", "test string")
	s := r.Context("test").(string)
	if s != "test string" {
		t.Fatalf("Could not retieve correct context '%s'", s)
	}
	r.SetContext("test2", TestType{"hello", "world"})
	tt := r.Context("test2").(TestType)
	if tt.One != "hello" || tt.Two != "world" {
		t.Fatalf("Could not retieve correct context '%+v'", tt)
	}
	tn := r.Context("test3")
	if tn != nil {
		t.Fatalf("Could not retieve correct context '%+v'", tn)
	}
}

func TestDomain(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		r.Write(r.Domain())
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	ts.URL += "/test.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode != 200 || string(body) != "localhost" {
		t.Fatalf("Get got status %d at %q, because 200 matching /test.html and body %s", r.StatusCode, url, body)
	}
}

func TestAuth(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		if _, _, ok := r.Credentials(); !ok {
			r.AskAuthentication("test")
		}
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	ts.URL += "/test.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode != 401 || strings.Trim(string(body), " \n\r") != "Unauthorized" {
		t.Fatalf("Get got status %d at %q, because 401 matching /test.html and body '%s'", r.StatusCode, url, strings.Trim(string(body), " \n\r"))
	}
}

func TestRedirect(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		r.Redirect("test2.html", false)
		return
	})
	rt.Get("/testp.html", func(r *RouteRequest) (err *Error) {
		r.Redirect("test2.html", true)
		return
	})
	rt.Get("/test2.html", func(r *RouteRequest) (err *Error) {
		return r.Write("redirect")
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	u := ts.URL + "/test.html"
	r, err := http.Get(u)
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	body, err := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode != 200 || string(body) != "redirect" {
		t.Fatalf("Get got status %d at %q, because 302 matching /test.html and body '%s'", r.StatusCode, url, body)
	}

	u = ts.URL + "/testp.html"
	r, err = http.Get(u)
	if err != nil {
		t.Fatal(err)
	}
	url = r.Request.URL.String()
	body, _ = ioutil.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode != 200 || string(body) != "redirect" {
		t.Fatalf("Get got status %d at %q, because 301 matching /testp.html and body '%s'", r.StatusCode, url, body)
	}
}

func TestPathIP(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		auth := r.Header("X-Auth-Token")
		if auth != "test-auth" {
			return NewError(500, "no header read")
		}
		return r.Write(r.Path() + " " + r.IPAddress())
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	u := ts.URL + "/test.html"
	var b io.Reader
	req, err := http.NewRequest("GET", u, b)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("X-Auth-Token", "test-auth")
	c := http.Client{}
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode != 200 || string(body) != "/test.html 127.0.0.1" {
		t.Fatalf("Get got status %d at %q, because 200 matching /test.html and body '%s'", r.StatusCode, url, body)
	}
}

func TestCookies(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		cookies := r.Cookies()
		if len(cookies) > 0 {
			return NewError(500, fmt.Sprintf("There should be no cookies %+v", cookies))
		}
		if c, err := r.Cookie("test"); err == nil || c != nil {
			return NewError(500, fmt.Sprintf("There should be no test cookie with err %s and cookie %+v", err, c))
		}
		// Set cookie
		cookie := &http.Cookie{
			Name:    "test",
			Value:   "test",
			Expires: time.Now().AddDate(1, 0, 0),
			Path:    "/test.html",
			// Domain:  r.Domain(),
		}
		r.SetCookie(cookie)

		return r.Write(r.Path() + " " + r.IPAddress())
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	u := ts.URL + "/test.html"
	j, err := cookiejar.New(nil)
	if err != nil {
		t.Fatalf("Could not initialize cookie jar with err %s", err)
	}

	client := &http.Client{Jar: j}
	r, err := client.Get(u)
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()
	var hasCookie bool
	for k, v := range r.Header {
		if k == "Set-Cookie" && strings.HasPrefix(v[0], "test=test;") {
			hasCookie = true
		}
	}
	if r.StatusCode != 200 || !hasCookie {
		t.Fatalf("Get got status %d at %q, because 200 matching /test.html and no cookie found '%s'", r.StatusCode, url, body)
	}
}

type TestJSON struct {
	Test string `json:"test"`
	Foo  string `json:"foo"`
	Mark string `json:"mark"`
}

func TestReadJSON(t *testing.T) {
	rt := NewRouter(nil)
	rt.AddErrorHandler(func(w http.ResponseWriter, r *http.Request, err *Error) {
		fmt.Fprintf(w, "%v", err)
	})
	rt.Post("/test.html", func(r *RouteRequest) (err *Error) {
		var data TestJSON
		e := r.ReadJSON(&data)
		if e != nil {
			return NewError(500, e.Error())
		}
		r.RawPostString()
		return r.WriteJSON(data)
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.html$")
	ts.URL += "/test.html"
	json := `{"test":"yes","foo":"bar","mark":"freese"}`
	r, err := http.Post(ts.URL, "application/json", strings.NewReader(json))
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != json {
		t.Fatalf("Get got status %d at %q, because 200 matching /test.html and body %s", r.StatusCode, url, string(body))
	}
}

type TestXML struct {
	XMLName xml.Name `xml:"element"`

	One string `xml:"one"`
	Two string `xml:"two"`
}

func TestReadXML(t *testing.T) {
	rt := NewRouter(nil)
	rt.Post("/test.html", func(r *RouteRequest) (err *Error) {
		var t TestXML
		if e := r.ReadXML(&t); e != nil {
			return NewError(500, e.Error())
		}
		if t.One != "een" && t.Two != "twee" {
			return NewError(500, fmt.Sprintf("Read xml failed %+v", t))
		}
		return r.WriteXML(t)
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	u := ts.URL + "/test.html"
	client := &http.Client{}
	xml := `<element><one>een</one><two>twee</two></element>`
	r, err := client.Post(u, "application/xml", strings.NewReader(xml))
	if err != nil {
		t.Fatal(err)
	}
	url := r.Request.URL.String()
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode != 200 || string(body) != xml {
		t.Fatalf("Get got status %d at %q, because 200 matching /test.html and xml %s", r.StatusCode, url, string(body))
	}
}
