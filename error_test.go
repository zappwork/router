package router

import (
	"fmt"
	"reflect"
	"testing"
)

func TestErrorInit(t *testing.T) {
	ei := Error{Code: 500, Message: "test error"}
	e := NewError(500, "test error")
	if !reflect.DeepEqual(e, &ei) {
		t.Fatalf("Error initialization is not correct %+v", ei)
	}
}

func TestErrorFromError(t *testing.T) {
	e := FromError(fmt.Errorf("test error"))
	if e == nil || e.String() != "test error" {
		t.Fatalf("Error from error is not correct %+v", e)
	}
	e = FromError(nil)
	if e != nil {
		t.Fatal("Error from nil error is not correct")
	}
}

func TestErrorError(t *testing.T) {
	ei := Error{Code: 500, Message: "test error"}
	err := ei.Error()
	if err == nil || err.Error() != "test error" {
		t.Fatalf("Error from error is not correct %+v", err)
	}
}

type TestStruct struct{ Test string }

func TestJSONError(t *testing.T) {
	m := TestStruct{Test: "hello"}
	e := NewJSONError(500, m)
	if e.String() != "{\"Test\":\"hello\"}" {
		t.Fatalf("JSON Error from error is not correct %+v", e)
	}
}
