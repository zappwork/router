package router

import (
	"encoding/json"
	"fmt"
)

// Error generic router error
type Error struct {
	Code    int
	Message string
}

// NewError return an initialized pointer to an
// a router error
func NewError(code int, message string) *Error {
	err := Error{
		Code:    code,
		Message: message,
	}
	return &err
}

// NewJSONError returns an initialized error where the message interface
// is marshalled as and json string
func NewJSONError(code int, message interface{}) *Error {
	m, e := json.Marshal(message)
	if e != nil {
		return NewError(500, e.Error())
	}
	return NewError(code, string(m))
}

// FromError makes a router error form a regular error
func FromError(err error) *Error {
	if err == nil {
		return nil
	}
	return NewError(500, err.Error())
}

// String returns a string representation of the error
func (e Error) String() string {
	return e.Message
}

// Error returns a error representation of the router error
func (e Error) Error() error {
	return fmt.Errorf(e.Message)
}
