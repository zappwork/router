# ZAPPWORK - Go Router #

[![codecov.io](https://codecov.io/bitbucket/zappwork/router/coverage.svg?branch=master)](https://codecov.io/bitbucket/zappwork/router?branch=master)
[![Go Report Card](https://goreportcard.com/badge/bitbucket.org/zappwork/router)](https://goreportcard.com/report/bitbucket.org/zappwork/router)
[![Build Status](https://drone.io/bitbucket.org/zappwork/router/status.png)](https://drone.io/bitbucket.org/zappwork/router/latest)
[![GoDoc](https://godoc.org/bitbucket.org/zappwork/router?status.svg)](https://godoc.org/bitbucket.org/zappwork/router)

## Installation ##

The current stable release is 0.8.1. This is a breaking release with 0.7.*, 0.6.* and 0.5. No more breaking changes will be added before final release.

Major changes in 0.8.0:
* Add multiple routers
* Route groups
* Additional request information methods
* Custom logger
* bug fixes

```BASH
go get bitbucket.org/zappwork/router
```

## Documentation ##
After installation run the following command in your terminal

Find documentation at [GoDoc](https://godoc.org/bitbucket.org/zappwork/router)

```BASH
godoc -http=:6060
```

and open the following url in your browser
[http://localhost:6060/pkg/bitbucket.org/zappwork/router/](http://localhost:6060/pkg/bitbucket.org/zappwork/router/)

## Usage ##
Usage is simple and easy to set up.

* Serve static files
* Serve static folder
* Routing
	* Custom HTTP Method
	* Generic routing
	* Grouped routes
	* Parameter routing
	* Catch routes
* Middleware (Use Globally, Grouped or on a route individualy)
* Logger
* NotFound
* Error

### Serve Static Files ###
You have the ability to serve single static files from any location. If you have only a few files this is the preferred route.

*Directory structure*

```
app
├── main.go
├── assets
|   ├── index.html
|   ├── favicon.ico
|   ├── css
|   |   └── styles.css
|   ├── js
|   |   └── main.js
|   ├── img
|   |   └── logo.png
```

*main.go*

```go
package main

import (
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.StaticFile("/",             "./assets/index.html")
	rt.StaticFile("/favicon.ico",  "./assets/favicon.ico")
	rt.StaticFile("/css/site.css", "./assets/css/styles.css")
	rt.StaticFile("/js/site.js",   "./assets/js/main.js")
	rt.StaticFile("/img/site.png", "./assets/img/logo.png")
	rt.Start(nil, true)
}
```

Now your files are being served.

### Serve Static Folders ###
You have the ability to serve static files from folders from any location. If you have only a few a lot of static files this is the preferred route.

*Directory structure*

```
app
├── main.go
├── assets
|   ├── index.html
|   ├── favicon.ico
|   ├── css
|   |	└── site.css
|   |	└── print.css
|   |	└── screen.css
|   ├── js
|   |	└── main.js
|   |	└── tools.js
|   |	└── mobile.js
|   ├── img
|   |	└── logo.png
|   |	└── logo-large.png
|   |	└── gallery-1.png
|   |	└── gallery-2.png
|   |	└── gallery-3.png
```

*main.go*

```go
package main

import (
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.StaticFile("/",             "./assets/index.html")
	rt.StaticFile("/favicon.ico",  "./assets/favicon.ico")
	rt.StaticFolder("/css/",       "./assets/css/")
	rt.StaticFolder("/js/",        "./assets/js/")
	rt.StaticFolder("/img/",       "./assets/img/")
	rt.Start(router.NewConfig(), true)
}
```

Any file in the folder will be served without the need to change your code.

### Routing ###
You do not want to use Go just for serving static files. Use the easy and efficient routing and map the requests easily for the HTTP request methods (e.g. GET, POST, PUT, DELETE, OPTIONS or HEAD). If no match is found for static files and routes then a 404 will be automatically generated.


#### Custom HTTP Method ####
Not all HTTP request methods are supported by all programming languages. That is why there are a few ways to fake an HTTP request method.

*Header X-HTTP-Method-Override*
Sometimes the "X-HTTP-Method-Override" header is used for the unsupported request method.

*Post/Form Value*
Another way to override the request method is through the "_METHOD" post/form value.

The zappwork router supports this ways of sending the request method.


#### Generic routing ####

*Routing functions*

```go
// Catch request for file.html and request method GET
rt.Get("/file.html", func(r *router.RouteRequest) (err *router.Error) {
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
	return
})
// Catch request for file.html and request method POST
rt.Post("/file.html", func(r *router.RouteRequest) (err *router.Error) {
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
	return
})
// Catch request for file.html and request method PUT
rt.Put("/file.html", func(r *router.RouteRequest) (err *router.Error) {
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
	return
})
// Catch request for file.html and request method DELETE
rt.Delete("/file.html", func(r *router.RouteRequest) (err *router.Error) {
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
	return
})
// Catch request for file.html and request method OPTIONS
rt.Options("/file.html", func(r *router.RouteRequest) (err *router.Error) {
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
	return
})
// Catch request for file.html and request method PATCH
rt.Patch("/file.html", func(r *router.RouteRequest) (err *router.Error) {
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
	return
})
```

*main.go*

```go
package main

import (
	"fmt"
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.Get("/", echo)
	rt.Get("/folder/", echo)
	rt.Get("/another/folder/", echo)
	rt.Get("/another/folder/hello.html", echo)
	rt.Start(router.NewConfig(), true)
}

// echo simple information
func echo(r *router.RouteRequest) (err *router.Error) {
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path())
	return
}
```

Now the paths are served through the router and will return the given information.


#### Parameter routing ####
It is also possible to add named routes with parameters in it. The parameters are returned in the RouteRequest object and can be used.

*main.go*

```go
package main

import (
	"fmt"
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.Get("/{name}.html", echo)
	rt.Get("/folder/{name}.html", echo)
	rt.Get("/another/{name}/", echo)
	rt.Get("/another/folder/{name}.html", echo)
	rt.Start(router.NewConfig(), true)
}

// echo simple information
func echo(r *router.RouteRequest) (err *router.Error) {
	name, _ := r.PathArg("name")
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path()+" "+name)
	return
}
```

Now the paths are served through the router and will return the given information.


#### Catch routes ####
Catch a route independant of the HTTP request method. First matches will be made to the specified request method and then the routes in the catch will be matched.

*main.go*

```go
package main

import (
	"fmt"
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.Get("/{name}.html", echo)
	rt.Catch("/{name}.html", echo)
	rt.Start(router.NewConfig(), true)
}

// echo simple information
func echo(r *router.RouteRequest) (err *router.Error) {
	name, _ := r.PathArg("name")
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path()+" "+name)
	return
}
```

Everything except the GET requests are processed by catch call.


### Middleware ###
It is also possible to add middleware for route pre-processing. The middeware can be added in with use function and requests can be manipulated and are used for route matching an processing. This functionality can be used for:

* Authentication
* URL rewriting
* Logging
* ...

*main.go*

```go
package main

import (
	"fmt"
	"log"
	"net/http"
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.Get("/{name}.html", echo)
	rt.Catch("/{name}.html", echo)
	rt.Use(rewrite)
	rt.Start(router.NewConfig(), true)
}

// echo simple information
func echo(r *router.RouteRequest) (err *router.Error) {
	name, _ := r.PathArg("name")
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path()+" "+name)
	return
}

// log simple information
func logger(r *router.RouteRequest) error {
	log.Println(r.Path(), " - ", r.Method())
	return nil
}

// Rewrite path
func rewrite(r *router.RouteRequest) error {
	r.Request().URL.Path = "/rewritten.html"
	return nil
}
```

### Logger ###
You are able to change the logging of the router and the custom logger should have the Logger interface.

```go
// This is the default logger
rt.SetLogger(router.DefaultLogger{})
```

### 404 Not Found ###
If a path is not matched in any way there will always be a standard 404 return from the router. The standard 404 can be overwritten with a custom 404 handler. The 404 status is return automatically.

```go
package main

import (
	"fmt"
	"net/http"
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.Get("/{name}.html", echo)
	rt.Catch("/{name}.html", echo)
	rt.AddNotFoundHandler(notfound)
	rt.Start(router.NewConfig(), true)
}

// echo simple information
func echo(r *router.RouteRequest) (err *router.Error) {
	name, _ := r.PathArg("name")
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path()+" "+name)
	return
}

// Simple 404 handler
func notfound(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf("I do not exist!")
}
```

### 500 Error ###
If an err was returned in one of the routing calls a standard server error was generated. This can also be overwritten in the router. A status 500 is automatically generated by the system.

```go
package main

import (
	"fmt"
	"errors"
	"net/http"
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.Get("/{name}.html", echo)
	rt.Catch("/{name}.html", echo)
	rt.AddErrorHandler(errorhandler)
	rt.Start(router.NewConfig(), true)
}

// echo simple information
func echo(r *router.RouteRequest) (err *router.Error) {
	return errors.New("Something went wrong!")
}

// Simple error handler
func errorhandler(w http.ResponseWriter, r *http.Request, errors ...error) {
	fmt.Fprintf("What went wrong")
}
```

### Custom Start & Stop output/function ###
Override the default start and stop output

```go
package main

import (
	"fmt"
	"errors"
	"net/http"
	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.Get("/{name}.html", echo)
	rt.Catch("/{name}.html", echo)
	rt.OnStart = func() {
		log.Println("Router started")
	}
	router.OnStop = func(message string) {
		log.Println("Router stopped with", message)
	}
	rt.Start(router.NewConfig(), true)
}

// echo simple information
func echo(r *router.RouteRequest) (err *router.Error) {
	name, _ := r.PathArg("name")
	fmt.Fprintf(r.Writer(), r.Method()+" "+r.Path()+" "+name)
	err = errors.New("Something went wrong!")
	return
}

// Simple 404 handler
func errorhandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf("What went wrong")
}
```
