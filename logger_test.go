package router

import "testing"

func TestLogger(t *testing.T) {
	l := DefaultLogger{}
	l.Request("uuid", "method", "path", 200, "200ms", ":::1", "mus", nil)
	l.Info("info")
	l.Infof("info %s", "really")
	l.Debug("debug")
	l.Debugf("debug %s", "really")
	l.Error("error")
	l.Errorf("error %s", "really")
}
