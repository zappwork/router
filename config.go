package router

import (
	"fmt"
	"time"
)

// Config foro router package
type Config struct {
	Host             string
	Port             int
	StaticTimeout    int
	RequestTimeout   time.Duration
	TimeoutMessage   string
	Logging          bool
	Verbose          bool
	ShowSystemInfo   bool
	CorsEnabled      bool
	CorsAllowOrigin  string
	CorsAllowHeaders string
	CorsAllowMethods string
}

// NewConfig initialise a config object
func NewConfig() *Config {
	return &Config{
		Host:           "",
		Port:           8080,
		Logging:        true,
		Verbose:        false,
		ShowSystemInfo: false,
		StaticTimeout:  0,
		RequestTimeout: 0,
		TimeoutMessage: "Request timeout",
	}
}

// HostString get the host string for the server
func (c *Config) HostString() string {
	return fmt.Sprintf("%s:%d", c.Host, c.Port)
}

// Strings get the strings of the config
func (c *Config) Strings() []string {
	var l []string
	l = append(l, fmt.Sprintf("%-14s %s:%d", "Host", c.Host, c.Port))
	l = append(l, fmt.Sprintf("%-14s %t", "Logging", c.Logging))
	l = append(l, fmt.Sprintf("%-14s %d", "StaticTimeout", c.StaticTimeout))
	l = append(l, fmt.Sprintf("%-14s %d", "RequestTimeout", c.RequestTimeout))
	l = append(l, fmt.Sprintf("%-14s %t", "Verbose", c.Verbose))
	l = append(l, fmt.Sprintf("%-14s %t", "Logging", c.Logging))
	return l
}
