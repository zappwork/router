package router

import (
	"reflect"
	"strings"
	"testing"
)

func TestInit(t *testing.T) {
	ci := NewConfig()
	c := &Config{Port: 8080, Logging: true, Verbose: false, StaticTimeout: 0, TimeoutMessage: "Request timeout", ShowSystemInfo: false}
	if !reflect.DeepEqual(c, ci) {
		t.Fatalf("Config initialisation is not correct %+v", ci)
	}
}

func TestHostString(t *testing.T) {
	c := NewConfig()
	if c.HostString() != ":8080" {
		t.Fatalf("HostString is not correct %s", c.HostString())
	}
}

func TestString(t *testing.T) {
	c := NewConfig()
	if strings.Join(c.Strings(), " ") != "Host           :8080 Logging        true StaticTimeout  0 RequestTimeout 0 Verbose        false Logging        true" {
		t.Fatalf("String is not correct %s", strings.Join(c.Strings(), " "))
	}
}
