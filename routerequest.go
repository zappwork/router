package router

import (
	"bytes"
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"regexp"
	"strconv"
	"strings"
	"time"
)

// RouteRequest struct used in the handler function
type RouteRequest struct {
	w           http.ResponseWriter
	r           *http.Request
	args        map[string]string
	context     map[string]interface{}
	queryParams url.Values
	d           []byte
	formParsed  bool
	statusCode  int
}

// Writer get the response writer to handle the response
func (rr *RouteRequest) Writer() http.ResponseWriter {
	return rr.w
}

// Request get the original call request. This should not be needed for regular
// requests, but can be used if needed.
func (rr *RouteRequest) Request() *http.Request {
	return rr.r
}

// Method get the response writer to handle the response
func (rr *RouteRequest) Method() string {
	return requestMethod(rr.r)
}

// Accept returns the type of response requested
func (rr *RouteRequest) Accept() string {
	return acceptType(rr.r)
}

// Host get the Host value
func (rr *RouteRequest) Host() string {
	return rr.r.Host
}

// Credentials get the credetials for authentication
func (rr *RouteRequest) Credentials() (user, password string, ok bool) {
	return rr.r.BasicAuth()
}

// AskAuthentication ask credentials for the given realm
func (rr *RouteRequest) AskAuthentication(realm string) {
	rr.Writer().Header().Set("WWW-Authenticate", fmt.Sprintf(`Basic realm=%s`, realm))
	http.Error(rr.Writer(), http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
}

// Header get a request header
func (rr *RouteRequest) Header(h string) string {
	return rr.r.Header.Get(h)
}

// AddHeader to the response
func (rr *RouteRequest) AddHeader(name, value string) {
	rr.Writer().Header().Set(name, value)
}

// Redirect from RouteRequest to the given path
func (rr *RouteRequest) Redirect(path string, permanent bool) {
	code := 302
	if permanent {
		code = 301
	}
	http.Redirect(rr.Writer(), rr.Request(), path, code)
}

// Path get the request path
func (rr *RouteRequest) Path() string {
	return rr.r.URL.Path
}

// Domain return the tld for the current host
func (rr *RouteRequest) Domain() string {
	domainParts := strings.Split(rr.Host(), ".")
	var domain string
	if len(domainParts) == 1 {
		domain = domainParts[0]
	} else {
		topParts := 2
		if domainParts[len(domainParts)-1] == "uk" {
			topParts = 3
		}
		domain = fmt.Sprintf(".%s", strings.Join(domainParts[len(domainParts)-topParts:], "."))
	}
	if strings.Contains(domain, ":") {
		domainParts = strings.Split(domain, ":")
		domain = domainParts[0]
	}

	if domain == ".0.1" {
		domain = "localhost"
	}

	return domain
}

// IPAddress get the remote IP and port used
func (rr *RouteRequest) IPAddress() string {
	return ParseRemoteAddr(rr.r)
}

// Cookie returns the named cookie provided in the request or ErrNoCookie if not found
func (rr *RouteRequest) Cookie(name string) (*http.Cookie, error) {
	return rr.Request().Cookie(name)
}

// Cookies parses and returns the HTTP cookies sent with the request
func (rr *RouteRequest) Cookies() []*http.Cookie {
	return rr.Request().Cookies()
}

// SetCookie adds a Set-Cookie header to the provided ResponseWriter's headers
func (rr *RouteRequest) SetCookie(cookie *http.Cookie) {
	http.SetCookie(rr.Writer(), cookie)
}

// SetContext sets a context key value pairing
func (rr *RouteRequest) SetContext(key string, value interface{}) {
	if rr.context == nil {
		rr.context = make(map[string]interface{})
	}
	rr.context[key] = value
}

// Context gets a context key value pairing
func (rr *RouteRequest) Context(key string) interface{} {
	if v, ok := rr.context[key]; ok {
		return v
	}
	return nil
}

// PathArg get the route/path arguments which were dynamic. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PathArg(key string) (arg string, ok bool) {
	arg, ok = rr.args[key]
	return
}

// PathArgAsBool get the post params posted in the from. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PathArgAsBool(key string) (bool, bool) {
	s, ok := rr.args[key]
	if !ok {
		return false, false
	}
	if b, err := strconv.ParseBool(s); err == nil {
		return b, true
	}
	return false, false
}

// PathArgAsInt get the post params posted in the from. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PathArgAsInt(key string) (int, bool) {
	s, ok := rr.args[key]
	if !ok {
		return 0, false
	}
	if i, err := strconv.Atoi(s); err == nil {
		return i, true
	}
	return 0, false
}

// PathArgAsInt64 get the route/path arguments which were dynamic. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PathArgAsInt64(key string) (arg int64, ok bool) {
	s, ok := rr.args[key]
	if !ok {
		return 0, false
	}
	if arg, err := strconv.ParseInt(s, 10, 64); err == nil {
		return arg, ok
	}
	return
}

// HasQueryParam does the request have a query param
func (rr *RouteRequest) HasQueryParam(key string) bool {
	_, ok := rr.QueryParam(key)
	return ok
}

// QueryParam get the query params as given in the url. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) QueryParam(key string) (string, bool) {
	if rr.queryParams == nil {
		rr.queryParams = rr.r.URL.Query()
	}
	if args, ok := rr.queryParams[key]; ok {
		if len(args) == 1 {
			return args[0], true
		}
	}
	return "", false
}

// QueryParamAsInt64 get query param as an int64
func (rr *RouteRequest) QueryParamAsInt64(key string) (int64, bool) {
	if rr.queryParams == nil {
		rr.queryParams = rr.r.URL.Query()
	}
	if args, ok := rr.queryParams[key]; ok {
		if len(args) == 1 {
			if i, err := strconv.ParseInt(args[0], 10, 64); err == nil {
				return i, true
			}
		}
	}
	return 0, false
}

// QueryParamAsUint64 get query param as an uint64
func (rr *RouteRequest) QueryParamAsUint64(key string) (uint64, bool) {
	if rr.queryParams == nil {
		rr.queryParams = rr.r.URL.Query()
	}
	if args, ok := rr.queryParams[key]; ok {
		if len(args) == 1 {
			if i, err := strconv.ParseUint(args[0], 10, 64); err == nil {
				return i, true
			}
		}
	}
	return 0, false
}

// QueryParamAsFloat64 get query param as an float64
func (rr *RouteRequest) QueryParamAsFloat64(key string) (float64, bool) {
	if rr.queryParams == nil {
		rr.queryParams = rr.r.URL.Query()
	}
	if args, ok := rr.queryParams[key]; ok {
		if len(args) == 1 {
			if f, err := strconv.ParseFloat(args[0], 64); err == nil {
				return f, true
			}
		}
	}
	return 0, false
}

// QueryParamAsBool get query param as an float64
func (rr *RouteRequest) QueryParamAsBool(key string) (bool, bool) {
	if rr.queryParams == nil {
		rr.queryParams = rr.r.URL.Query()
	}
	if args, ok := rr.queryParams[key]; ok {
		if len(args) == 1 {
			if b, err := strconv.ParseBool(args[0]); err == nil {
				return b, true
			}
		}
	}
	return false, false
}

// QueryParamAsTime get query param as a time.Time
func (rr *RouteRequest) QueryParamAsTime(key, layout string) (*time.Time, bool) {
	if rr.queryParams == nil {
		rr.queryParams = rr.r.URL.Query()
	}
	if args, ok := rr.queryParams[key]; ok {
		if len(args) == 1 {
			if t, err := time.Parse(layout, args[0]); err == nil {
				return &t, true
			}
		}
	}
	return nil, false
}

// QueryParamAsSlice get query param as an float64
func (rr *RouteRequest) QueryParamAsSlice(key, seperator string) (s []string, ok bool) {
	if rr.queryParams == nil {
		rr.queryParams = rr.r.URL.Query()
	}
	if args, k := rr.queryParams[key]; k {
		ok = true
		if len(args) == 1 {
			s = strings.Split(args[0], ",")
		} else {
			s = args
		}
	}
	return
}

// PostValue get the post params posted in the from. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PostValue(key string) (string, bool) {
	if v := rr.r.PostFormValue(key); v != "" {
		return v, true
	}
	return "", false
}

// PostValueAsBool get the post params posted in the from. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PostValueAsBool(key string) (bool, bool) {
	if v := rr.r.PostFormValue(key); v != "" {
		if b, err := strconv.ParseBool(v); err == nil {
			return b, true
		}
	}
	return false, false
}

// PostValueAsInt get the post params posted in the from. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PostValueAsInt(key string) (int, bool) {
	if v := rr.r.PostFormValue(key); v != "" {
		if i, err := strconv.Atoi(v); err == nil {
			return i, true
		}
	}
	return 0, false
}

// PostValueAsInt64 get the post params posted in the form
func (rr *RouteRequest) PostValueAsInt64(key string) (int64, bool) {
	if v := rr.r.PostFormValue(key); v != "" {
		if i, err := strconv.ParseInt(v, 10, 64); err == nil {
			return i, true
		}
	}
	return 0, false
}

// PostValueAsFloat64 get the post params posted in the from. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PostValueAsFloat64(key string) (float64, bool) {
	if v := rr.r.PostFormValue(key); v != "" {
		if f, err := strconv.ParseFloat(v, 64); err == nil {
			return f, true
		}
	}
	return 0, false
}

// PostMultiValue get the post params posted in the from. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PostMultiValue(key string) ([]string, bool) {
	if !rr.formParsed {
		rr.formParsed = true
		rr.r.ParseForm()
	}
	if v, ok := rr.r.Form[key]; ok {
		return v, true
	}
	return nil, false
}

// The PostMap type
type PostMap map[string]string

// PostArrayMap get the post ArrayMap posted in the from. They are int -> key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PostArrayMap(key string) (arg map[int]PostMap, ok bool) {
	if !rr.formParsed {
		rr.formParsed = true
		rr.r.ParseForm()
	}
	ek := strings.Replace(key, "[", "\\[", -1)
	ek = strings.Replace(ek, "]", "\\]", -1)
	reg := fmt.Sprintf("^%s\\[(.*?)\\]\\[(.*?)\\]$", ek)
	pm, _ := regexp.Compile(reg)
	for k, val := range rr.r.Form {
		if matched, _ := regexp.MatchString(reg, k); !matched || strings.Count(k[len(key):], "[") != 2 {
			continue
		}

		ok = true
		p := pm.FindAllStringSubmatch(k, -1)
		key := p[0][1]
		if arg == nil {
			arg = make(map[int]PostMap)
		}

		ki, err := strconv.Atoi(key)
		if err != nil {
			continue
		}

		if _, ok := arg[ki]; !ok {
			arg[ki] = make(PostMap)
		}

		arg[ki][p[0][2]] = val[0]
	}
	return
}

// PostMap get the post ArrayMap posted in the from. They are key/value pair
// and can be retrieved through this function
func (rr *RouteRequest) PostMap(key string) (arg PostMap, ok bool) {
	if !rr.formParsed {
		rr.formParsed = true
		rr.r.ParseForm()
	}
	ek := strings.Replace(key, "[", "\\[", -1)
	ek = strings.Replace(ek, "]", "\\]", -1)
	reg := fmt.Sprintf("^%s\\[(.*?)\\]$", ek)
	pm, _ := regexp.Compile(reg)

	for k, val := range rr.r.Form {
		if matched, _ := regexp.MatchString(reg, k); !matched || strings.Count(k[len(key):], "[") != 1 {
			continue
		}

		ok = true
		p := pm.FindAllStringSubmatch(k, -1)
		key := p[0][1]
		if arg == nil {
			arg = make(PostMap)
		}
		arg[key] = val[0]
	}
	return
}

// RawPostBytes get posted body as a string.
func (rr *RouteRequest) RawPostBytes() (data []byte, err error) {
	if rr.d != nil {
		data = rr.d
		return
	}
	data, err = ioutil.ReadAll(rr.r.Body)
	rr.r.Body.Close()
	if err == nil {
		rr.d = data
	}
	return
}

// RawPostString get posted body as a byte slice.
func (rr *RouteRequest) RawPostString() (data string, err error) {
	d, err := rr.RawPostBytes()
	if err == nil {
		data = string(d)
	}
	return
}

// SetStatusCode set the return status code
func (rr *RouteRequest) SetStatusCode(code int) {
	rr.statusCode = code
}

// WriteStatusCode set write the status code
func (rr *RouteRequest) WriteStatusCode(code int) {
	if code > 0 {
		rr.SetStatusCode(code)
	}
	if rr.StatusCode() > 0 {
		rr.Writer().WriteHeader(rr.statusCode)
	}
}

// StatusCode get the status code
func (rr *RouteRequest) StatusCode() int {
	return rr.statusCode
}

// UnmarshalRawPost get Unmarshal posted json into an interface.
func (rr *RouteRequest) UnmarshalRawPost(v interface{}) (err error) {
	d, err := rr.RawPostBytes()
	if err == nil {
		err = json.Unmarshal(d, &v)
	}
	return
}

// ReadJSON get the raw Json information
func (rr *RouteRequest) ReadJSON(v interface{}) (err error) {
	return rr.UnmarshalRawPost(v)
}

// ReadXML get Unmarshal posted json into an interface.
func (rr *RouteRequest) ReadXML(v interface{}) (err error) {
	d, err := rr.RawPostBytes()
	if err == nil {
		err = xml.Unmarshal(d, &v)
	}
	return
}

// Write interface as JSON to client
func (rr *RouteRequest) Write(s string) (err *Error) {
	if _, e := fmt.Fprint(rr.Writer(), s); e != nil {
		err = NewError(500, e.Error())
	}
	return
}

// WriteBytes write bytes to client
func (rr *RouteRequest) WriteBytes(b []byte) (err *Error) {
	return rr.Write(string(b))
}

// WriteBuffer write bytes buffer to client
func (rr *RouteRequest) WriteBuffer(b *bytes.Buffer) (err *Error) {
	if _, e := b.WriteTo(rr.Writer()); e != nil {
		err = NewError(500, e.Error())
	}
	return
}

// WriteJSON write interface as JSON to client
func (rr *RouteRequest) WriteJSON(v interface{}) (err *Error) {
	response, e := json.Marshal(v)
	if e != nil {
		return NewError(500, e.Error())
	}
	return rr.Write(string(response))
}

// WriteXML write interface as XML to client
func (rr *RouteRequest) WriteXML(v interface{}) (err *Error) {
	response, e := xml.Marshal(v)
	if e != nil {
		return NewError(500, e.Error())
	}
	return rr.Write(string(response))
}
