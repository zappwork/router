package router

import (
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"regexp"
	"testing"
	"time"
)

func TestStaticFile(t *testing.T) {
	rt := NewRouter(nil)
	rt.StaticFile("test.txt", "test/test.txt")
	ts := httptest.NewUnstartedServer(rt.Handler())
	//Set Handler to http.DefaultServeMux for testing other http handlers
	// ts.Config.Handler = http.DefaultServeMux
	ts.Start()
	defer ts.Close()

	rt.config.StaticTimeout = 3600

	var end = regexp.MustCompile("/test.txt$")
	ts.URL += "/test.txt"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "This is a test text file." {
		t.Fatalf("Got status %d at %q, because not status 200 matching /test.txt with body %s $", r.StatusCode, url, string(body))
	}
}

func TestStaticFolder(t *testing.T) {
	rt := NewRouter(nil)
	rt.StaticFolder("css", "test/css/")
	ts := httptest.NewUnstartedServer(rt.Handler())
	//Set Handler to http.DefaultServeMux for testing other http handlers
	// ts.Config.Handler = http.DefaultServeMux
	ts.Start()
	defer ts.Close()

	var end = regexp.MustCompile("/css/test.css$")
	ts.URL += "/css/test.css"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "body { color: #999; }" {
		t.Fatalf("Got status %d at %q, because not status 200 matching /css/test.css with body %s $", r.StatusCode, url, string(body))
	}
}

func TestGetStaticRouting(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "test1")
		return
	})

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.html$")
	ts.URL += "/test.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "test1" {
		t.Fatalf("Got status %d at %q, because 200 matching /test.html$", r.StatusCode, url)
	}
}

func TestRequestTimeout(t *testing.T) {
	c := NewConfig()
	c.RequestTimeout = 10 * time.Millisecond
	rt := NewRouter(c)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		time.Sleep(5 * time.Second)
		fmt.Fprintf(r.Writer(), "test1")
		return
	})

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.html$")
	ts.URL += "/test.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 503 || !end.MatchString(url) || string(body) != "Request timeout" {
		t.Fatalf("Got status %d at %q, because 503 matching /test.html$", r.StatusCode, url)
	}
}

func TestCatchStaticRouting(t *testing.T) {
	rt := NewRouter(nil)
	rt.Catch("/test.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "test1")
		return
	})

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.html$")
	ts.URL += "/test.html"
	r, err := http.PostForm(ts.URL, url.Values{"key": {"Value"}, "id": {"123"}})
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "test1" {
		t.Fatalf("Got status %d at %q, because 200 matching /test.html$", r.StatusCode, url)
	}
}

func TestPostStaticRouting(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "gettest1")
		return
	})
	rt.Post("/test", func(r *RouteRequest) (err *Error) {
		var v string
		v, _ = r.PostValue("key")
		fmt.Fprintf(r.Writer(), "posttest1"+v)
		return
	})
	rt.Put("/test.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "puttest1")
		return
	})
	rt.Delete("/test.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "deletetest1")
		return
	})
	rt.Options("/test.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "optionstest1")
		return
	})
	rt.SetLogger(DefaultLogger{})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test$")
	ts.URL += "/test"
	r, err := http.PostForm(ts.URL, url.Values{"key": {"Value"}, "id": {"123"}})
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "posttest1Value" {
		t.Fatalf("Got status %d at %q, because 200 matching /test.html and body %s $", r.StatusCode, url, string(body))
	}
}

func TestPostStaticVars(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		var v string
		var q string
		v, _ = r.PostValue("key")
		q, _ = r.QueryParam("query")
		fmt.Fprintf(r.Writer(), "posttest1"+v+q)
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.html\\?query=testquery$")
	ts.URL += "/test.html?query=testquery"
	r, err := http.PostForm(ts.URL, url.Values{"_METHOD": {"GET"}, "key": {"Value"}, "id": {"123"}})
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "posttest1Valuetestquery" {
		t.Fatalf("Got status %d at %q, because 200 matching /test.html and body %s $", r.StatusCode, url, string(body))
	}
}

func TestPostDynamicRouting(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "gettest1")
		return
	})
	rt.Post("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		var v string
		var f string
		var fo string
		v, _ = r.PostValue("key")
		f, _ = r.PathArg("file")
		fo, _ = r.PathArg("folder")
		fmt.Fprintf(r.Writer(), "posttest1"+v+f+fo)
		return
	})
	rt.Put("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "puttest1")
		return
	})
	rt.Delete("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "deletetest1")
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/zappwork/test.html$")
	ts.URL += "/zappwork/test.html"
	r, err := http.PostForm(ts.URL, url.Values{"key": {"Value"}, "id": {"123"}})
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "posttest1Valuetestzappwork" {
		t.Fatalf("Got status %d at %q, because 200 matching /zappwork/test.html and body %s $", r.StatusCode, url, string(body))
	}
}

func TestDeleteDynamicRouting(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "gettest1")
		return
	})
	rt.Post("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "posttest1")
		return
	})
	rt.Put("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "puttest1")
		return
	})
	rt.Delete("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "deletetest1")
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/zappwork/test.html$")
	ts.URL += "/zappwork/test.html"
	var b io.Reader
	req, err := http.NewRequest("DELETE", ts.URL, b)
	c := http.Client{}
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "deletetest1" {
		t.Fatalf("Got status %d at %q, because 200 matching /zappwork/test.html and body %s $", r.StatusCode, url, string(body))
	}
}

func TestPutDynamicRouting(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "gettest1")
		return
	})
	rt.Post("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "posttest1")
		return
	})
	rt.Put("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "puttest1")
		return
	})
	rt.Delete("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "deletetest1")
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/zappwork/test.html$")
	ts.URL += "/zappwork/test.html"
	var b io.Reader
	req, err := http.NewRequest("PUT", ts.URL, b)
	c := http.Client{}
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "puttest1" {
		t.Fatalf("Got status %d at %q, because 200 matching /zappwork/test.html and body %s $", r.StatusCode, url, string(body))
	}
}

func TestPatchDynamicRouting(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "gettest1")
		return
	})
	rt.Post("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "posttest1")
		return
	})
	rt.Put("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "puttest1")
		return
	})
	rt.Delete("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "deletetest1")
		return
	})
	rt.Patch("/{folder}/{file}.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "patchtest1")
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/zappwork/test.html$")
	ts.URL += "/zappwork/test.html"
	var b io.Reader
	req, err := http.NewRequest("PATCH", ts.URL, b)
	c := http.Client{}
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "patchtest1" {
		t.Fatalf("Got status %d at %q, because 200 matching /zappwork/test.html and body %s $", r.StatusCode, url, string(body))
	}
}

func TestGetDynamicRouting(t *testing.T) {
	rt := NewRouter(nil)

	testFunc := func(r *RouteRequest) (err *Error) {
		if f, ok := r.PathArg("file"); ok {
			fmt.Fprintf(r.Writer(), f)
			return
		}
		err = NewError(500, "No path argument found")
		return
	}

	rt.Get("/test/{folder}/{file}.html", testFunc)
	rt.Get("/{folder}/{file}.html", testFunc)
	rt.Get("/{file}.html", testFunc)
	rt.Get("/test.html", testFunc)
	rt.Get("test", testFunc)

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test-url.html$")
	ts.URL += "/test-url.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "test-url" {
		t.Fatalf("Got status %d at %q, because 200 matching /test.html$", r.StatusCode, url)
	}
}

func TestCatchDynamicRouting(t *testing.T) {
	rt := NewRouter(nil)

	testFunc := func(r *RouteRequest) (err *Error) {
		if f, ok := r.PathArg("file"); ok {
			fmt.Fprintf(r.Writer(), f)
			return
		}
		err = NewError(500, "No path argument found")
		return
	}

	rt.Catch("/test/{folder}/{file}.html", testFunc)
	rt.Catch("/{folder}/{file}.html", testFunc)
	rt.Catch("/{file}.html", testFunc)
	rt.Catch("/test.html", testFunc)
	rt.Catch("test", testFunc)

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test-url.html$")
	ts.URL += "/test-url.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "test-url" {
		t.Fatalf("Got status %d at %q, because 200 matching /test.html$", r.StatusCode, url)
	}
}

func TestSimpleNotFound(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "test1")
		return
	})

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test$")
	ts.URL += "/test"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	r.Body.Close()
	url := r.Request.URL.String()
	if r.StatusCode != 404 || !end.MatchString(url) {
		t.Fatalf("Got status %d at %q, because 404 matching /test2.html$", r.StatusCode, url)
	}
}

func TestCustomNotFound(t *testing.T) {
	rt := NewRouter(nil)
	rt.AddNotFoundHandler(func(rr *RouteRequest) *Error {
		fmt.Fprintf(rr.Writer(), "haha you cannot find me")
		return nil
	})
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "test1")
		return
	})

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test2.html$")
	ts.URL += "/test2.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 404 || !end.MatchString(url) || string(body) != "haha you cannot find me" {
		t.Fatalf("Got status %d at %q, because 404 matching /test2.html and body %s $", r.StatusCode, url, string(body))
	}
}

func TestSimpleError(t *testing.T) {
	rt := NewRouter(nil)
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		// fmt.Fprintf(r.Writer(), "test1")
		err = NewError(500, "What happened?")
		return
	})

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.html$")
	ts.URL += "/test.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	r.Body.Close()
	url := r.Request.URL.String()
	if r.StatusCode != 500 || !end.MatchString(url) {
		t.Fatalf("Got status %d at %q, because 500 matching /test.html$", r.StatusCode, url)
	}
}

func TestCustomError(t *testing.T) {
	rt := NewRouter(nil)
	rt.AddErrorHandler(func(w http.ResponseWriter, r *http.Request, err *Error) {
		fmt.Fprintf(w, "What happened an Error?")
	})
	rt.Get("/test.html", func(r *RouteRequest) (err *Error) {
		err = NewError(500, "What happened?")
		return
	})

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.html$")
	ts.URL += "/test.html"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 500 || !end.MatchString(url) || string(body) != "What happened an Error?" {
		t.Fatalf("Got status %d at %q, because 500 matching /test.html and body %s $", r.StatusCode, url, string(body))
	}
}

func TestUse(t *testing.T) {
	rt := NewRouter(nil)
	rt.Use(func(r *RouteRequest) *Error {
		r.Request().URL.Path = "/nottest.xml"
		return NewError(500, "use failed")
	})
	rt.Get("/nottest.xml", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "gettest1")
		return
	})
	rt.Get("/test.txt", func(r *RouteRequest) (err *Error) {
		fmt.Fprintf(r.Writer(), "thisisatextfile")
		return
	})
	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test.txt$")
	ts.URL += "/test.txt"
	r, err := http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 500 || !end.MatchString(url) {
		t.Fatalf("Got status %d at %q, because not status 500 matching /test.txt with body %s $", r.StatusCode, url, string(body))
	}
}

func TestRouteGroup(t *testing.T) {
	rt := NewRouter(nil)
	rt.Group(
		NewGroup("/test").Get(".", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "get route")
			return
		}).Get("../", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "get route")
			return
		}).Get("index.html", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "get route")
			return
		}).Post("index.html", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "post route")
			return
		}).Delete("index.html", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "delete route")
			return
		}).Put("index.html", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "put route")
			return
		}).Patch("index.html", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "patch route")
			return
		}).Options("index.html", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "options route")
			return
		}).Catch("index.html", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "all route")
			return
		}).Group(
			NewGroup("/test2").Get("index.html", func(r *RouteRequest) (err *Error) {
				fmt.Fprintf(r.Writer(), "get route")
				return
			})))

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	testURL := ts.URL + "/test/index.html"
	testBaseURL := ts.URL + "/test/"
	testRGPost(testURL, t)
	testRGGet(testURL, t)
	testRGGet(testBaseURL, t)
	testRGPatch(testURL, t)
	testRGDelete(testURL, t)
	testRGPut(testURL, t)
	testRGOptions(testURL, t)
	testRGConnect(testURL, t)
}

func testRGPost(uri string, t *testing.T) {
	var end = regexp.MustCompile("/test/index.html$")
	r, err := http.PostForm(uri, url.Values{"key": {"Value"}, "id": {"123"}})
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "post route" {
		t.Fatalf("Got status %d at %q, because 200 matching /test/index.html$ - %s", r.StatusCode, url, string(body))
	}
}

func testRGGet(uri string, t *testing.T) {
	r, err := http.Get(uri)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	r.Body.Close()
	if r.StatusCode != 200 || string(body) != "get route" {
		t.Fatalf("Got status %d, because 200 - %s", r.StatusCode, string(body))
	}
}

func testRGDelete(uri string, t *testing.T) {
	var end = regexp.MustCompile("/test/index.html$")
	var b io.Reader
	req, _ := http.NewRequest("DELETE", uri, b)
	c := http.Client{}
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	u := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(u) || string(body) != "delete route" {
		t.Fatalf("Got status %d at %q, because 200 matching /test/index.html$ - %s", r.StatusCode, u, string(body))
	}
}

func testRGPatch(uri string, t *testing.T) {
	var end = regexp.MustCompile("/test/index.html$")
	var b io.Reader
	c := http.Client{}
	req, _ := http.NewRequest("PATCH", uri, b)
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	u := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(u) || string(body) != "patch route" {
		t.Fatalf("Got status %d at %q, because 200 matching /test/index.html$ - %s", r.StatusCode, u, string(body))
	}
}

func testRGPut(uri string, t *testing.T) {
	var end = regexp.MustCompile("/test/index.html$")
	var b io.Reader
	c := http.Client{}
	req, _ := http.NewRequest("PUT", uri, b)
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	u := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(u) || string(body) != "put route" {
		t.Fatalf("Got status %d at %q, because 200 matching /test/index.html$ - %s", r.StatusCode, u, string(body))
	}
}

func testRGOptions(uri string, t *testing.T) {
	var end = regexp.MustCompile("/test/index.html$")
	var b io.Reader
	c := http.Client{}
	req, _ := http.NewRequest("OPTIONS", uri, b)
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	u := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(u) || string(body) != "options route" {
		t.Fatalf("Got status %d at %q, because 200 matching /test/index.html$ - %s", r.StatusCode, u, string(body))
	}
}

func testRGConnect(uri string, t *testing.T) {
	var end = regexp.MustCompile("/test/index.html$")
	var b io.Reader
	c := http.Client{}
	req, _ := http.NewRequest("CONNECT", uri, b)
	r, err := c.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	body, _ := ioutil.ReadAll(r.Body)
	u := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(u) || string(body) != "all route" {
		t.Fatalf("Got status %d at %q, because 200 matching /test/index.html$ - %s", r.StatusCode, u, string(body))
	}
}

func TestNestedRouteGroup(t *testing.T) {
	rt := NewRouter(nil)
	rt.Group(
		NewGroup("/test").Catch("index.html", func(r *RouteRequest) (err *Error) {
			fmt.Fprintf(r.Writer(), "all route")
			return
		}).Group(
			NewGroup("/test2").Post("index.html", func(r *RouteRequest) (err *Error) {
				fmt.Fprintf(r.Writer(), "post nested route")
				return
			}).Get("index.html", func(r *RouteRequest) (err *Error) {
				fmt.Fprintf(r.Writer(), "post nested route")
				return
			}, func(r *RouteRequest) (err *Error) {
				return NewError(500, "middleware")
			})))

	ts := httptest.NewServer(rt.Handler())
	defer ts.Close()

	var end = regexp.MustCompile("/test/test2/index.html$")
	ts.URL += "/test/test2/index.html"
	r, err := http.PostForm(ts.URL, url.Values{"key": {"Value"}, "id": {"123"}})
	if err != nil {
		t.Fatal(err)
	}
	body, err := ioutil.ReadAll(r.Body)
	url := r.Request.URL.String()
	r.Body.Close()
	if r.StatusCode != 200 || !end.MatchString(url) || string(body) != "post nested route" {
		t.Fatalf("Got status %d at %q, because 200 matching /test/test2/index.html$ - %s", r.StatusCode, url, string(body))
	}

	r, err = http.Get(ts.URL)
	if err != nil {
		t.Fatal(err)
	}
	url = r.Request.URL.String()
	if r.StatusCode != 500 || !end.MatchString(url) {
		t.Fatalf("Got status %d at %q, because 500 matching /test/index.html$", r.StatusCode, url)
	}
}
