package main

import (
	"fmt"
	"time"

	"bitbucket.org/zappwork/router"
)

func main() {
	rt := router.NewRouter(nil)
	rt.StaticFile("/test.css", "../css/test.css")
	rt.Get("/", echo)
	c := router.NewConfig()
	c.Port = 8000
	c.ShowSystemInfo = false
	rt.Start(c, false)

	rt2 := router.NewRouter(nil)
	rt2.StaticFile("/test.css", "../test.txt")
	rt2.Get("/", echo2)
	rt2.Get("/sleep.html", sleep)
	c2 := router.NewConfig()
	c2.Port = 8001
	c2.ShowSystemInfo = false
	rt2.Start(c2, true)
}

func echo(r *router.RouteRequest) (err error) {
	name, _ := r.PathArg("name")
	fmt.Fprintf(r.Writer(), ":8000 - "+r.Method()+" "+r.Path()+" "+name)
	return
}

func echo2(r *router.RouteRequest) (err error) {
	name, _ := r.PathArg("name")
	fmt.Fprintf(r.Writer(), ":8001 - "+r.Method()+" "+r.Path()+" "+name)
	return
}

func sleep(r *router.RouteRequest) (err error) {
	time.Sleep(10 * time.Second)
	fmt.Fprintf(r.Writer(), ":8001 - "+r.Method()+" "+r.Path())
	return
}
