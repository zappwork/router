package router

import "log"

// Logger interface
type Logger interface {
	Request(string, string, string, int, string, string, string, *Error)
	Info(args ...interface{})
	Infof(format string, args ...interface{})
	Error(args ...interface{})
	Errorf(format string, args ...interface{})
	Debug(args ...interface{})
	Debugf(format string, args ...interface{})
}

// DefaultLogger is the default logger
type DefaultLogger struct{}

// Request logs the router request
func (d DefaultLogger) Request(uuid, method, path string, status int, duration, ip, useragent string, err *Error) {
	log.Printf("[REQUEST] %s - %15s - %d/%12s -> %s %s  (%s)", uuid, ip, status, duration, method, path, useragent)
}

// Info logs information
func (d DefaultLogger) Info(args ...interface{}) {
	args = append([]interface{}{"[INFO]"}, args...)
	log.Println(args...)
}

// Infof logs information
func (d DefaultLogger) Infof(format string, args ...interface{}) {
	log.Printf("[INFO] "+format+"\n", args...)
}

// Error logs error information
func (d DefaultLogger) Error(args ...interface{}) {
	args = append([]interface{}{"[ERROR]"}, args...)
	log.Println(args...)
}

// Errorf logs information
func (d DefaultLogger) Errorf(format string, args ...interface{}) {
	log.Printf("[ERROR] "+format+"\n", args...)
}

// Debug logs debug information
func (d DefaultLogger) Debug(args ...interface{}) {
	args = append([]interface{}{"[DEBUG]"}, args...)
	log.Println(args...)
}

// Debugf logs information
func (d DefaultLogger) Debugf(format string, args ...interface{}) {
	log.Printf("[DEBUG] "+format+"\n", args...)
}
